import 'package:flutter/material.dart';
import 'package:moneysender/pages/depart-list.page.dart';
import 'package:moneysender/pages/home.page.dart';
import 'package:moneysender/pages/my_parcels.page.dart';
import 'package:moneysender/pages/profil.page.dart';
import 'package:moneysender/utilities/constant.dart';
import 'package:moneysender/widgets/publish-stepper.dart';

class BaseScaffold extends StatefulWidget {
  const BaseScaffold({Key? key}) : super(key: key);

  @override
  State<BaseScaffold> createState() => _BaseScaffoldState();
}

class _BaseScaffoldState extends State<BaseScaffold> {
  AppBar? _appBar;
  final controller = PageController();

  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: _appBar,
      body: IndexedStack(
        index: _selectedIndex,
        children: [
          const HomePage(),
          const DepartListPage(),
          PublishStepper(),
          const MyParcelPage(),
          const ProfilPage(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Constant.mainGreenColor,
        elevation: 10,
        currentIndex: _selectedIndex,
        unselectedLabelStyle: const TextStyle(color: Colors.black),
        showUnselectedLabels: true,
        showSelectedLabels: true,
        selectedLabelStyle: const TextStyle(color: Colors.black),
        backgroundColor: Colors.white,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                const AssetImage("assets/search.png"),
                color: _selectedIndex == 0
                    ? Constant.mainGreenColor
                    : Constant.secondaryColor,
              ),
              label: 'rechercher'),
          BottomNavigationBarItem(
              icon: ImageIcon(
                const AssetImage("assets/navigation.png"),
                color: _selectedIndex == 1
                    ? Constant.mainGreenColor
                    : Constant.secondaryColor,
              ),
              label: 'autour de moi'),
          BottomNavigationBarItem(
              icon: ImageIcon(
                const AssetImage("assets/plus.png"),
                color: _selectedIndex == 2
                    ? Constant.mainGreenColor
                    : Constant.secondaryColor,
                size: 40,
              ),
              label: ''),
          BottomNavigationBarItem(
              icon: ImageIcon(
                const AssetImage("assets/travel-luggage.png"),
                color: _selectedIndex == 3
                    ? Constant.mainGreenColor
                    : Constant.secondaryColor,
              ),
              label: 'mes colis'),
          BottomNavigationBarItem(
              icon: ImageIcon(
                const AssetImage("assets/profile.png"),
                color: _selectedIndex == 4
                    ? Constant.mainGreenColor
                    : Constant.secondaryColor,
              ),
              label: 'profil'),
        ],
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
