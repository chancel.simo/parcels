class SearchRequest {
  String? departure;
  String? arrival;
  DateTime date;

  SearchRequest({
    this.departure,
    this.arrival,
    required this.date,
  });
}
