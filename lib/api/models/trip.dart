// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Trip {
  String? id;
  String? startPlace;
  String? arrivalPlace;
  int? kilo;
  DateTime? startDate;
  String? userId;
  String? planeTicketPath;
  String? parcelType;

  Trip({
    this.id,
    this.startPlace,
    this.arrivalPlace,
    this.kilo,
    this.startDate,
    this.userId,
    this.planeTicketPath,
    this.parcelType,
  });

  Map<String, dynamic> _toMap() {
    return <String, dynamic>{
      'id': id,
      'startPlace': startPlace,
      'arrivalPlace': arrivalPlace,
      'kilo': kilo,
      'startDate': startDate?.millisecondsSinceEpoch,
      'userId': userId,
      'planeTicketPath': planeTicketPath,
      'parcelType': parcelType,
    };
  }

  factory Trip.fromMap(Map<String, dynamic> map) {
    return Trip(
        id: map['id'] != null ? map['id'] as String : null,
        startPlace:
            map['startPlace'] != null ? map['startPlace'] as String : null,
        arrivalPlace:
            map['arrivalPlace'] != null ? map['arrivalPlace'] as String : null,
        kilo: map['kilo'] != null ? map['kilo'] as int : null,
        startDate: map['startDate'] != null
            ? DateTime.fromMillisecondsSinceEpoch(map['startDate'] as int)
            : null,
        userId: map['userId'] != null ? map['userId'] as String : null,
        planeTicketPath: map['planeTicketPath'] != null
            ? map['planeTicketPath'] as String
            : null,
        parcelType: map['parcelType'] != null
            ? (map['parcelType'] as Enum).toString()
            : null);
  }

  String toJson() => json.encode(_toMap());

  factory Trip.fromJson(String source) =>
      Trip.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Trip &&
        other.id == id &&
        other.startPlace == startPlace &&
        other.arrivalPlace == arrivalPlace &&
        other.kilo == kilo &&
        other.startDate == startDate &&
        other.userId == userId &&
        other.planeTicketPath == planeTicketPath &&
        other.parcelType == parcelType;
  }
}
