import 'package:flutter/material.dart';
import 'package:moneysender/utilities/constant.dart';
import 'package:paged_vertical_calendar/paged_vertical_calendar.dart';
import 'package:intl/intl.dart';

class DatePickerPage extends StatefulWidget {
  const DatePickerPage({Key? key}) : super(key: key);

  @override
  State<DatePickerPage> createState() => _DatePickerPageState();
}

class _DatePickerPageState extends State<DatePickerPage> {
  final DateTime _currentDate = DateTime.now();
  DateTime? _dateSelected;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white12,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: const Text(
          "Selectioner une date",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 20),
            child: PagedVerticalCalendar(
              initialDate: _currentDate,
              listPadding: const EdgeInsets.only(top: 10, bottom: 10),
              dayBuilder: (context, date) {
                final bool isCurrentDate = _currentDate.day == date.day &&
                    _currentDate.month == date.month;

                final bool isSelectedDate = (_dateSelected?.day == date.day &&
                    _dateSelected?.month == date.month);

                final bool isPastDate =
                    date.isBefore(DateTime.now()) && !isCurrentDate;

                final currentDateColor = isCurrentDate
                    ? Constant.mainGreenColor
                    : Colors.transparent;

                final selectedDateColor = isSelectedDate && !isPastDate
                    ? Constant.mainGreenColor
                    : Colors.transparent;

                return Container(
                  decoration: BoxDecoration(
                      color: selectedDateColor,
                      border: Border.all(color: currentDateColor),
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                    child: Text(
                      DateFormat('d').format(date),
                      style: TextStyle(
                          color: isSelectedDate
                              ? Colors.white
                              : (isPastDate ? Colors.grey : Colors.black)),
                    ),
                  ),
                );
              },
              onDayPressed: (date) {
                final bool isCurrentDate = _currentDate.day == date.day &&
                    _currentDate.month == date.month;

                final bool isPastDate =
                    date.isBefore(DateTime.now()) && !isCurrentDate;
                if (isPastDate) return;
                setState(() {
                  _dateSelected = date;
                });
                Navigator.pop(context, date);
              },
            ),
          ),
        ),
      ),
    );
  }
}
