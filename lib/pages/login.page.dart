import 'package:flutter/material.dart';
import 'package:moneysender/utilities/constant.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constant.appBgColor,
      appBar: null,
      body: Column(
        children: const [
          Text(
            "Hello",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}
