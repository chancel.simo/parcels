// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:moneysender/api/models/model.dart';
import 'package:moneysender/utilities/constant.dart';
import 'package:intl/intl.dart';
import 'package:moneysender/widgets/custom-btn.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final SearchRequest _searchRequest = SearchRequest(date: DateTime.now());

  @override
  void initState() {
    super.initState();
  }

  String formatSelectedDate(DateTime date) {
    if (date.day == DateTime.now().day && date.month == DateTime.now().month) {
      return "Aujourdhui";
    } else {
      return DateFormat('dd/MM/yyyy').format(date);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: const BoxDecoration(color: Colors.white12),
      child: Stack(
        children: [
          Column(
            children: [
              Stack(
                children: [
                  Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.4,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/bg-home.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 100, left: 30, right: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Flexible(
                            child: Stack(children: [
                              Text(
                                "Trouvez un voyageur pour la livraison de vos colis",
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    foreground: Paint()
                                      ..style = PaintingStyle.stroke
                                      ..strokeWidth = 2
                                      ..color = Colors.black),
                                textAlign: TextAlign.center,
                              ),
                              Text(
                                "Trouvez un voyageur pour la livraison de vos colis",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 120, left: 20, right: 20),
                child: Column(
                  children: [
                    MainPrimaryButton(
                      onPressed: () {
                        debugPrint("test btn");
                      },
                      title: "Rechercher",
                    ),
                    LayoutBuilder(builder: (context, constraint) {
                      return Container(
                        margin: const EdgeInsets.only(top: 20),
                        width: constraint.maxWidth,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Mes trajets favoris",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                IconButton(
                                  onPressed: () {
                                    debugPrint("hello");
                                  },
                                  icon: Icon(Icons.arrow_forward_ios_rounded),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: constraint.maxWidth,
                              height: 150,
                              child: Center(
                                child: Text(
                                  'Aucun départ pour le moment',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    })
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            top: 200,
            child: Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.29,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.05),
                        spreadRadius: 4,
                        blurRadius: 4,
                        offset: Offset(0, 1))
                  ],
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                ),
                child: LayoutBuilder(builder: (context, constraint) {
                  return ListView(
                    children: [
                      ListTile(
                        leading: SizedBox(
                          width: constraint.maxWidth * 0.8,
                          child: Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Row(
                              children: [
                                Icon(Icons.location_on),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                    _searchRequest.departure ?? "Départ",
                                    style: TextStyle(
                                        color: _searchRequest.departure == null
                                            ? Colors.grey
                                            : Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () async {
                          final response = await Navigator.pushNamed(
                              context, RouteList.autoComplete);
                          if (response is String) {
                            setState(() {
                              _searchRequest.departure = response;
                            });
                          }
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      ListTile(
                        leading: SizedBox(
                          width: constraint.maxWidth * 0.8,
                          child: Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Row(
                              children: [
                                Icon(Icons.location_on),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                    _searchRequest.departure ?? "Destination",
                                    style: TextStyle(
                                        color: _searchRequest.arrival == null
                                            ? Colors.grey
                                            : Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () async {
                          final response = await Navigator.pushNamed(
                              context, RouteList.autoComplete);
                          if (response is String) {
                            setState(() {
                              _searchRequest.arrival = response;
                            });
                          }
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      ListTile(
                        leading: SizedBox(
                          width: constraint.maxWidth * 0.7,
                          child: Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Row(
                              children: [
                                Icon(Icons.calendar_today_outlined),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                    formatSelectedDate(_searchRequest.date),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () async {
                          final response = await Navigator.pushNamed(
                              context, RouteList.datepicker);
                          if (response is DateTime) {
                            setState(() {
                              _searchRequest.date = response;
                            });
                          }
                        },
                      ),
                    ],
                  );
                }),
              ),
            ),
          )
        ],
      ),
    );
  }
}
