// ignore_for_file: must_be_immutable

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:moneysender/api/models/trip.dart';
import 'package:moneysender/utilities/constant.dart';
import 'package:moneysender/widgets/alert-selectable-dialog.dart';
import 'package:moneysender/widgets/custom-btn.dart';
import 'package:moneysender/widgets/edit-widget.dart';
import 'package:paged_vertical_calendar/paged_vertical_calendar.dart';

class PublishPage extends StatefulWidget {
  final String questionLabel;
  final String? hintLabel;
  final bool? isNumeric;
  Function onValueSubmit;
  Function? onBackButton;
  final bool showBackButton;
  final bool isAdressAutoComplete;
  final bool isSelectableList;
  final bool showValidateBtn;
  final bool isDocumentUpload;
  final bool isPickDate;
  final bool largeDisplay;
  PublishPage({
    required this.questionLabel,
    required this.onValueSubmit,
    this.onBackButton,
    this.hintLabel,
    this.isNumeric,
    this.showBackButton = false,
    this.isAdressAutoComplete = false,
    this.isSelectableList = false,
    this.showValidateBtn = false,
    this.isDocumentUpload = false,
    this.isPickDate = false,
    this.largeDisplay = false,
    Key? key,
  }) : super(key: key);

  @override
  State<PublishPage> createState() => _PublishPageState();
}

class _PublishPageState extends State<PublishPage> {
  List<String>? _result;
  String? selectedResult;
  File? ticketPlaneImage;

  @override
  void initState() {
    super.initState();
  }

  Function _onEdit() => (String value) {
        if (widget.isAdressAutoComplete) {
          setState(() {
            _result = Constant.cities
                .where((element) =>
                    value.length > 1 &&
                    element.toLowerCase().contains(value.toLowerCase()))
                .toList();
          });
        } else {
          selectedResult = value;
        }
      };

  IconButton _backButton() {
    return IconButton(
      onPressed: () {
        widget.onBackButton!();
      },
      icon: const Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
    );
  }

  Widget _formWidget() {
    if (widget.isSelectableList) {
      return EditWidget(
        handleValue: (value) {
          debugPrint(value);
        },
        hintLabel: "Selectionner un ou plusieurs type(s)",
        selectedValue: selectedResult,
        handleTap: () async {
          showDialog(
            context: context,
            builder: (_) => const AlertSelectableDialog(
                title: "Selectionnez un ou plusieurs types"),
          );
        },
        suffixBtn: IconButton(
          onPressed: () async {
            var response = await showDialog<List<CheckboxWrapper>>(
              context: context,
              builder: (_) => const AlertSelectableDialog(
                  title: "Selectionnez un ou plusieurs types"),
            );

            if (response != null) {
              setState(() {
                selectedResult = response.map((e) => e.label).join(",");
              });
            }
          },
          icon: const Icon(
            Icons.arrow_drop_down_rounded,
            color: Colors.black,
            size: 30,
          ),
        ),
      );
    } else if (widget.isDocumentUpload) {
      return Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Column(
          children: [
            const Text(
              "Sélectionnez un ou plusieurs documents",
              style: TextStyle(
                fontSize: 15,
              ),
            ),
            const Text(
              "JPEG, PNG, JPG, PDF.",
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Image.asset(
                'assets/ticket.png',
                scale: 1.5,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: MainPrimaryButton(
                onPressed: () {
                  showCupertinoModalPopup(
                    context: context,
                    builder: (BuildContext context) => CupertinoActionSheet(
                        title: const Text('choisir un mode.'),
                        actions: <CupertinoActionSheetAction>[
                          CupertinoActionSheetAction(
                            onPressed: () {
                              pickImage(ImageSource.camera);
                            },
                            child: const Text('Prendre une photo'),
                          ),
                          CupertinoActionSheetAction(
                            onPressed: () {
                              pickImage(ImageSource.gallery);
                            },
                            child: const Text('Ouvrir la galerie'),
                          ),
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: const Text(
                            'Annuler',
                            style: TextStyle(color: Colors.red),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )),
                  );
                },
                title: "uploader",
                color: Constant.secondaryColor,
                icon: const Icon(Icons.file_upload_outlined),
              ),
            ),
          ],
        ),
      );
    } else if (widget.isPickDate) {
      return PagedVerticalCalendar(
        startDate: DateTime.now(),
        onDayPressed: (date) {
          widget.onValueSubmit(Trip(startDate: date));
        },
      );
    } else {
      return EditWidget(
        handleValue: _onEdit(),
        hintLabel: widget.hintLabel ?? "Saisissez l'adresse exacte",
        isNumeric: widget.isNumeric ?? false,
        selectedValue: selectedResult,
      );
    }
  }

  Widget addressAutocomplete(BoxConstraints constraints) {
    return SizedBox(
      height: constraints.maxHeight * 0.8,
      child: _result != null && _result!.isNotEmpty
          ? ListView.separated(
              shrinkWrap: true,
              itemBuilder: (context, index) {
                final String place = _result![index];
                return Padding(
                  padding: const EdgeInsets.only(top: 0),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        selectedResult = place;
                      });
                      widget.onValueSubmit(
                          Trip(startPlace: place, arrivalPlace: place));
                    },
                    child: ListTile(
                      leading: SizedBox(
                        height: 30,
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: Text(place,
                            style: const TextStyle(color: Colors.black)),
                      ),
                      trailing: const Icon(Icons.arrow_forward_ios_outlined),
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) => const Divider(
                color: Colors.grey,
              ),
              itemCount: _result!.length,
            )
          : const Padding(
              padding: EdgeInsets.only(top: 20),
              child: Center(
                child: Text(
                  "Les resultats apparaitront ici.",
                  style: TextStyle(color: Constant.secondaryColor),
                ),
              ),
            ),
    );
  }

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);

      if (image == null) return;

      final imageTmp = File(image.path);
      setState(() {
        ticketPlaneImage = imageTmp;
      });
    } on PlatformException catch (e) {
      debugPrint(e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 0, left: 15, right: 15),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.8,
          child: LayoutBuilder(builder: (context, constraints) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: constraints.maxHeight * 0.10,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0, bottom: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            if (widget.showBackButton) _backButton(),
                            Flexible(
                              child: Text(
                                widget.questionLabel,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 21,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: constraints.maxHeight *
                          (!widget.largeDisplay ? 0.10 : 0.8),
                      child: _formWidget(),
                    ),
                    if (widget.isAdressAutoComplete)
                      addressAutocomplete(constraints),
                  ],
                ),
                if (widget.showValidateBtn)
                  MainPrimaryButton(
                    onPressed: () {
                      if (widget.isSelectableList) {
                        widget.onValueSubmit(Trip(parcelType: selectedResult));
                      } else {
                        widget.onValueSubmit(
                          Trip(
                            kilo: int.parse(selectedResult!),
                          ),
                        );
                      }
                    },
                    title: "Valider",
                  ),
              ],
            );
          }),
        ),
      ),
    );
  }
}
