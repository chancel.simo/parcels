// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:moneysender/utilities/constant.dart';

class AutocompletePage extends StatefulWidget {
  const AutocompletePage({Key? key}) : super(key: key);

  @override
  State<AutocompletePage> createState() => _AutocompletePageState();
}

class _AutocompletePageState extends State<AutocompletePage> {
  final TextEditingController _textEditingController = TextEditingController();
  List<String>? _result;

  String? searchValue;

  @override
  void initState() {
    super.initState();
    _result = [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white12,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            setState(() {
              FocusManager.instance.primaryFocus?.unfocus();
            });
            Navigator.pop(context);
          },
        ),
        title: TextField(
          autofocus: true,
          textAlign: TextAlign.start,
          textAlignVertical: TextAlignVertical.bottom,
          cursorColor: Constant.mainGreenColor,
          cursorHeight: 22,
          controller: _textEditingController,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Rechercher',
            hintStyle: TextStyle(
              textBaseline: TextBaseline.alphabetic,
              letterSpacing: 3,
              fontSize: 15,
            ),
          ),
          onChanged: (value) {
            debugPrint(value);
            setState(() {
              _result = Constant.cities
                  .where((element) =>
                      value.length > 1 &&
                      element.toLowerCase().contains(value.toLowerCase()))
                  .toList();

              debugPrint(_result.toString());
            });
          },
        ),
      ),
      body: SingleChildScrollView(
        child: _result != null && _result!.isNotEmpty
            ? ListView.separated(
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  final String item = _result![index];
                  return Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context, item);
                      },
                      child: ListTile(
                        leading: SizedBox(
                          height: 30,
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: Text(
                            item,
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                        trailing: Icon(Icons.arrow_forward_ios_outlined),
                      ),
                    ),
                  );
                },
                itemCount: _result!.length,
                separatorBuilder: (context, index) => Divider(
                  color: Constant.secondaryColor,
                ),
              )
            : Center(
                child: Text(
                  "Les resultats apparaitront ici.",
                  style: TextStyle(color: Constant.secondaryColor),
                ),
              ),
      ),
    );
  }
}
