import 'package:flutter/material.dart';
import 'package:moneysender/base/base.scaffold.dart';
import 'package:moneysender/pages/autocomplete.page.dart';
import 'package:moneysender/pages/date-picker.page.dart';
import 'package:moneysender/pages/depart-list.page.dart';
import 'package:moneysender/pages/home.page.dart';
import 'package:moneysender/pages/login.page.dart';
import 'package:moneysender/pages/my_parcels.page.dart';
import 'package:moneysender/pages/profil.page.dart';
import 'package:moneysender/utilities/constant.dart';
import 'package:moneysender/utilities/data.provider.dart';
import 'package:moneysender/widgets/publish-stepper.dart';
import 'package:provider/provider.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    // bool isConnected = Provider.of<DataProvider>(context).isConnected

    switch (settings.name) {
      case RouteList.base:
        return MaterialPageRoute(builder: (_) => const BaseScaffold());
      case RouteList.home:
        return MaterialPageRoute(builder: (_) => const HomePage());
      case RouteList.autoComplete:
        return MaterialPageRoute(builder: (_) => const AutocompletePage());
      case RouteList.datepicker:
        return MaterialPageRoute(builder: (_) => const DatePickerPage());
      case RouteList.publish:
        return MaterialPageRoute(builder: (_) => PublishStepper());
      case RouteList.departList:
        return MaterialPageRoute(builder: (_) => const DepartListPage());
      case RouteList.login:
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case RouteList.myParcels:
        return MaterialPageRoute(builder: (_) => const MyParcelPage());
      case RouteList.profil:
        return MaterialPageRoute(builder: (_) => const ProfilPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return const Scaffold(
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
