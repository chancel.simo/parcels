import 'package:flutter/material.dart';

class DataProvider with ChangeNotifier {
  bool isConnected;

  DataProvider({this.isConnected = false});

  void setConnectivity(bool isConnectedValue) {
    isConnected = isConnectedValue;

    notifyListeners();
  }
}
