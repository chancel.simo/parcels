import 'package:flutter/material.dart';

class Constant {
  static const BoxDecoration gradientAppbar = BoxDecoration(
    gradient: LinearGradient(
      stops: [
        0.10,
        0.90,
      ],
      begin: Alignment.bottomLeft,
      end: Alignment.topRight,
      colors: [
        Color(0xFF74EAF1),
        Color(0xFF0CA7A7),
      ],
    ),
  );

  static const Color mainGreenColor = Color(0xFF0CA7A7);
  static const Color secondaryColor = Colors.grey;
  static const Color appBgColor = Colors.white12;

  static List<String> cities = [
    'Marseille',
    'Paris',
    'Lyon',
    'Casablanca',
    'Douala',
    'Marrakech',
    'Porto'
  ];
}

class RouteList {
  static const String base = '/';
  static const String home = '/home';
  static const String autoComplete = '/autocomplete';
  static const String datepicker = '/datepicker';
  static const String publish = '/publish';
  static const String departList = 'departlist';
  static const String login = '/login';
  static const String myParcels = '/myparcel';
  static const String profil = '/profil';
}
