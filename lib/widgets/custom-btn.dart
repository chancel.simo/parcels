import 'package:flutter/material.dart';
import 'package:moneysender/utilities/constant.dart';

class MainPrimaryButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final Color color;
  final Icon? icon;
  const MainPrimaryButton({
    required this.onPressed,
    required this.title,
    this.color = Constant.mainGreenColor,
    this.icon,
    Key? key,
  }) : super(key: key);

  @override
  State<MainPrimaryButton> createState() => _MainPrimaryButtonState();
}

class _MainPrimaryButtonState extends State<MainPrimaryButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(widget.color),
          minimumSize: MaterialStateProperty.all(const Size(80, 50)),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ))),
      onPressed: () {
        widget.onPressed();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            widget.title,
            style: const TextStyle(color: Colors.white),
          ),
          if (widget.icon != null) widget.icon!
        ],
      ),
    );
  }
}
