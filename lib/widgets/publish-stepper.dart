import 'package:flutter/material.dart';
import 'package:moneysender/api/models/trip.dart';
import 'package:moneysender/pages/publish.page.dart';
import 'package:moneysender/utilities/constant.dart';

class PublishStepper extends StatefulWidget {
  int stepperIndex;
  PublishStepper({this.stepperIndex = 0, Key? key}) : super(key: key);

  @override
  State<PublishStepper> createState() => _PublishStepperState();
}

class _PublishStepperState extends State<PublishStepper> {
  final PageController _pageController = PageController();
  final Trip _trip = Trip();

  @override
  void initState() {
    super.initState();
  }

  Function onSubmit() => (Trip trip) {
        switch (_pageController.page!.round()) {
          case 0:
            setState(() {
              _trip.startPlace = trip.startPlace;
            });
            break;
          case 1:
            setState(() {
              _trip.arrivalPlace = trip.arrivalPlace;
            });
            break;
          case 2:
            setState(() {
              _trip.startDate = trip.startDate;
            });
            break;
          case 3:
            setState(() {
              _trip.parcelType = trip.parcelType;
            });
            break;
          case 4:
            if (_isOnlyCourrier(_trip)) {
              setState(() {
                _trip.kilo = trip.kilo;
              });
            } else {
              setState(() {
                _trip.planeTicketPath = trip.planeTicketPath;
                widget.stepperIndex++;
              });
            }
            break;
          case 5:
            setState(() {
              _trip.planeTicketPath = trip.planeTicketPath;
            });
            break;

          default:
        }
        setState(() {
          widget.stepperIndex++;
        });
        _pageController.nextPage(
            duration: const Duration(milliseconds: 200),
            curve: Curves.easeInQuad);
      };

  bool _isOnlyCourrier(Trip trip) {
    return trip.parcelType!
        .split(",")
        .any((element) => element == "valise" || element == "colis");
  }

  Function onBackButton() => () {
        setState(() {
          widget.stepperIndex--;
        });
        _pageController.previousPage(
            duration: const Duration(milliseconds: 200), curve: Curves.easeIn);
      };

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: LayoutBuilder(builder: (context, constraints) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: constraints.maxHeight * 0.01,
                child: LinearProgressIndicator(
                  backgroundColor: Constant.secondaryColor,
                  color: Constant.mainGreenColor,
                  value: ((widget.stepperIndex + 1) / 6),
                ),
              ),
              SizedBox(
                height: constraints.maxHeight * 0.99,
                child: PageView(
                  controller: _pageController,
                  physics: const NeverScrollableScrollPhysics(),
                  children: <PublishPage>[
                    PublishPage(
                      questionLabel: "Quelle est votre ville de départ ?",
                      onValueSubmit: onSubmit(),
                      isAdressAutoComplete: true,
                    ),
                    PublishPage(
                      questionLabel: "Quelle est votre destination ?",
                      onValueSubmit: onSubmit(),
                      showBackButton: true,
                      onBackButton: onBackButton(),
                      isAdressAutoComplete: true,
                    ),
                    PublishPage(
                      questionLabel: "Quelle est la date de votre départ ?",
                      onValueSubmit: onSubmit(),
                      isPickDate: true,
                      largeDisplay: true,
                      showBackButton: true,
                    ),
                    PublishPage(
                      questionLabel:
                          "Quel type de colis souhaitez-vous transporter ?",
                      onValueSubmit: onSubmit(),
                      showBackButton: true,
                      isSelectableList: true,
                      onBackButton: onBackButton(),
                      showValidateBtn: true,
                    ),
                    if (_trip.parcelType != null)
                      _isOnlyCourrier(_trip)
                          ? PublishPage(
                              questionLabel:
                                  "Combien de kilogramme(KG) pouvez-vous transporter ?",
                              onValueSubmit: onSubmit(),
                              hintLabel: "Saisir le nombre de kilogramme",
                              isNumeric: true,
                              showBackButton: true,
                              onBackButton: onBackButton(),
                              showValidateBtn: true,
                            )
                          : PublishPage(
                              questionLabel:
                                  "Ajoutez un document pour confirmer votre voyage.",
                              onValueSubmit: onSubmit(),
                              showBackButton: true,
                              onBackButton: onBackButton(),
                              showValidateBtn: true,
                              isDocumentUpload: true,
                              largeDisplay: true,
                            ),
                    if (_trip.planeTicketPath == null)
                      PublishPage(
                        questionLabel:
                            "Ajoutez un document pour confirmer votre voyage.",
                        onValueSubmit: onSubmit(),
                        showBackButton: true,
                        onBackButton: onBackButton(),
                        showValidateBtn: true,
                        isDocumentUpload: true,
                        largeDisplay: true,
                      ),
                  ],
                ),
              ),

              /* IndexedStack(
                  children: [
                    PublishPage(
                      questionLabel: "Quelle est votre ville de départ ?",
                      onValueSubmit: onSubmit(),
                      isAdressAutoComplete: true,
                    ),
                    PublishPage(
                      questionLabel: "Quelle est votre destination ?",
                      onValueSubmit: onSubmit(),
                      showBackButton: true,
                      onBackButton: onBackButton(),
                      isAdressAutoComplete: true,
                    ),
                    PublishPage(
                      questionLabel:
                          "Quel type de colis souhaitez-vous transporter ?",
                      onValueSubmit: onSubmit(),
                      showBackButton: true,
                      isSelectableList: true,
                      onBackButton: onBackButton(),
                      showValidateBtn: true,
                    ),
                    PublishPage(
                      questionLabel:
                          "Combien de kilogramme(KG) pouvez-vous transporter ?",
                      onValueSubmit: onSubmit(),
                      hintLabel: "Saisir le nombre de kilogramme",
                      isNumeric: true,
                      showBackButton: true,
                      onBackButton: onBackButton(),
                      showValidateBtn: true,
                    ),
                    /* PublishPage(
                      questionLabel: "Ajoutez un document pour confirmer votre voyage.",
                      onValueSubmit: onSubmit(),
                      showBackButton: true,
                      onBackButton: onBackButton(),
                      showValidateBtn: true,
                      isDocumentUpload: true,
                    ), */
                  ],
                  index: widget.stepperIndex,
                ), */
            ],
          );
        }),
      ),
    );
  }
}
