import 'package:flutter/material.dart';
import 'package:moneysender/utilities/constant.dart';

class CustomBottomBar extends StatefulWidget {
  const CustomBottomBar({Key? key}) : super(key: key);

  @override
  State<CustomBottomBar> createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      selectedItemColor: Constant.mainGreenColor,
      elevation: 10,
      currentIndex: _selectedIndex,
      unselectedLabelStyle: const TextStyle(color: Colors.black),
      showUnselectedLabels: true,
      showSelectedLabels: true,
      selectedLabelStyle: const TextStyle(color: Colors.black),
      backgroundColor: Colors.white,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: ImageIcon(
              const AssetImage("assets/search.png"),
              color: _selectedIndex == 0
                  ? Constant.mainGreenColor
                  : Constant.secondaryColor,
            ),
            label: 'rechercher'),
        BottomNavigationBarItem(
            icon: ImageIcon(
              const AssetImage("assets/navigation.png"),
              color: _selectedIndex == 1
                  ? Constant.mainGreenColor
                  : Constant.secondaryColor,
            ),
            label: 'autour de moi'),
        BottomNavigationBarItem(
            icon: ImageIcon(
              const AssetImage("assets/plus.png"),
              color: _selectedIndex == 2
                  ? Constant.mainGreenColor
                  : Constant.secondaryColor,
              size: 40,
            ),
            label: ''),
        BottomNavigationBarItem(
            icon: ImageIcon(
              const AssetImage("assets/travel-luggage.png"),
              color: _selectedIndex == 3
                  ? Constant.mainGreenColor
                  : Constant.secondaryColor,
            ),
            label: 'mes colis'),
        BottomNavigationBarItem(
            icon: ImageIcon(
              const AssetImage("assets/profile.png"),
              color: _selectedIndex == 4
                  ? Constant.mainGreenColor
                  : Constant.secondaryColor,
            ),
            label: 'profil'),
      ],
      onTap: (index) {
        setState(() {
          _selectedIndex = index;
        });
      },
    );
  }
}
