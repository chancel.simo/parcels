import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moneysender/utilities/constant.dart';

class AlertSelectableDialog extends StatefulWidget {
  final String title;
  const AlertSelectableDialog({required this.title, Key? key})
      : super(key: key);

  @override
  State<AlertSelectableDialog> createState() => _AlertSelectableDialogState();
}

class _AlertSelectableDialogState extends State<AlertSelectableDialog> {
  final List<CheckboxWrapper> _checkboxWrapper = [
    CheckboxWrapper(label: "courrier"),
    CheckboxWrapper(label: "valise"),
    CheckboxWrapper(label: "colis"),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(
        widget.title,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 15,
        ),
        textAlign: TextAlign.start,
      ),
      content: Material(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              for (CheckboxWrapper item in _checkboxWrapper)
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Checkbox(
                        checkColor: Colors.white,
                        fillColor: MaterialStateProperty.resolveWith(
                            (states) => Constant.mainGreenColor),
                        value: item.isChecked,
                        onChanged: (value) {
                          setState(() {
                            item.isChecked = value!;
                          });
                        },
                      ),
                    ),
                    Expanded(
                        flex: 3,
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              item.isChecked = !item.isChecked;
                            });
                          },
                          child: Text(
                            item.label,
                            style: const TextStyle(color: Colors.black),
                          ),
                        ))
                  ],
                ),
              Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Center(
                    child: TextButton(
                      child: const Text(
                        "Valider",
                        style: TextStyle(color: Constant.mainGreenColor),
                      ),
                      onPressed: () {
                        // ignore: prefer_null_aware_operators
                        var _parcelTypeChecked = _checkboxWrapper
                            .where((element) => element.isChecked)
                            .toList();
                        Navigator.of(context).pop(_parcelTypeChecked);
                      },
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

class CheckboxWrapper {
  bool isChecked;
  final String label;

  CheckboxWrapper({
    required this.label,
    this.isChecked = false,
  });
}
