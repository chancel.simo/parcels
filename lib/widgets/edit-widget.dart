import 'package:flutter/material.dart';
import 'package:moneysender/utilities/constant.dart';

class EditWidget extends StatefulWidget {
  Function handleValue;
  Function? handleTap;
  final String? hintLabel;
  final bool isNumeric;
  final IconButton? suffixBtn;
  String? selectedValue;

  EditWidget(
      {required this.handleValue,
      this.hintLabel,
      this.isNumeric = false,
      this.suffixBtn,
      this.selectedValue,
      this.handleTap,
      Key? key})
      : super(key: key);

  @override
  State<EditWidget> createState() => _EditWidgetState();
}

class _EditWidgetState extends State<EditWidget> {
  // final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          initialValue: widget.selectedValue ?? '',
          autofocus: true,
          cursorColor: Constant.mainGreenColor,
          keyboardType: widget.isNumeric
              ? TextInputType.number
              : TextInputType.streetAddress,
          decoration: InputDecoration(
            hintText: widget.hintLabel,
            fillColor: Colors.grey.shade300,
            filled: true,
            border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(15),
            ),
            suffixIcon: widget.suffixBtn,
          ),
          onChanged: (value) {
            widget.handleValue(value);
          },
          onTap: () {
            widget.handleTap;
          },
        ),
      ],
    );
  }
}
